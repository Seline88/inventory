$(function() {
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('#year-filter-slider').bootstrapSlider();
	$('#msrp-filter-slider').bootstrapSlider();

	$('#update-entries-button').click(function() {
		var entries = {};
		$('.entry-checkbox:checked').each(function() {
			const tr = $(this).parent().parent();

			var entry = {};
			entry['vin_nr'] = tr.find('.vin-nr').find('input').val();
			entry['model'] = tr.find('.model').find('input').val();
			entry['maker'] = tr.find('.maker').find('input').val();
			entry['year'] = tr.find('.year').find('input').val();
			entry['msrp'] = tr.find('.msrp').find('input').val();
			entry['status'] = tr.find('.status').find('select').val();

			const bookedVal = tr.find('.booked').find('select').val();
			if (bookedVal === 'Yes') {
				entry['booked'] = '1';
			} else if (bookedVal === 'No') {
				entry['booked'] = '0';
			}

			const listedVal = tr.find('.listed').find('select').val();
			if (listedVal === 'Yes') {
				entry['listed'] = '1';
			} else if (listedVal === 'No') {
				entry['listed'] = '0';
			}
			
			entries[tr.find('.id').text()] = entry;
		});

		updateInventoryEntries(entries);
	});

	$('#select-all-entries-checkbox').change(function() {
		const selectAllCheckedStatus = $(this).prop('checked');
		$('.entry-checkbox').each(function() {
			if ($(this).prop('checked') != selectAllCheckedStatus) {
				$(this).prop('checked', selectAllCheckedStatus).change();
			}
		});
	});

	$('#delete-entries-button').click(function() {
		var ids = [];
		$('.entry-checkbox:checked').each(function() {
			ids.push($(this).parent().parent().find('.id').text());
		});

		deleteInventoryEntries(ids);
	});

	$('#update-inventory-filter-button').click(function() {
		getInventoryEntries();
	});

	$('#clear-inventory-filter-button').click(function() {
		clearFilter();
	});

	$('.site-menu-button').click(function () {
        $('.site-menu-content').toggleClass('side-menu-left-open');
        var overlay = $(document.createElement('div'));
        overlay.attr('class', 'menu-background-overlay');
        overlay.hide();
        $('body').append(overlay);
        overlay.fadeIn(150);
    });

    $('.inventory-filter-button').click(function () {
        $('.inventory-filter-content').toggleClass('side-menu-right-open');
        var overlay = $(document.createElement('div'));
        overlay.attr('class', 'menu-background-overlay');
        overlay.hide();
        $('body').append(overlay);
        overlay.fadeIn(150);
    });

    $(document).mouseup(function (e) {
	    var leftSideMenu = $('.site-menu-content');
	    var rightSideMenu = $('.inventory-filter-content');

	    var missed = 0;
	    if (!leftSideMenu.is(e.target) && leftSideMenu.has(e.target).length === 0) {
	        leftSideMenu.removeClass('side-menu-left-open');
	        missed++;
	    }
	    if (!rightSideMenu.is(e.target) && rightSideMenu.has(e.target).length === 0) {
	        rightSideMenu.removeClass('side-menu-right-open');
	        missed++;
	    }

	    if (missed === 2) {
	    	$('.menu-background-overlay').fadeOut(150, function() {
		    	$('.menu-background-overlay').remove();
		    });
	    }	    
	});

	init();
});

function init() {
	$('.entry-checkbox').change(function() {
		if ($(this).prop('checked')) {
			transformIntoInput($(this), '.vin-nr');
			transformIntoInput($(this), '.model');
			transformIntoInput($(this), '.maker');
			transformIntoInput($(this), '.year');
			transformIntoInput($(this), '.msrp');
			transformIntoSelect($(this), '.status');
			transformIntoSelect($(this), '.booked');
			transformIntoSelect($(this), '.listed');
		} else {
			$(this).parent().parent().find('.edit-entry').each(function() {
				this.replaceWith($(this).val());
			});
		}
	});
}

function getInventoryEntries() {
	const filter = getFilterValues();
	$('#table-update-overlay').fadeIn();
	var query = '';
	if (filter !== undefined && Object.keys(filter).length > 0) {
		query = $.param(filter);
	}
	$.ajax({
       	type: 'GET',
       	url: 'get-inventory-entries',
       	data: query,
       	success: function(entryViews) {
       		$('#inventory-entries').html(entryViews);
       		$('#select-all-entries-checkbox').prop('checked', false);
       		$('#table-update-overlay').fadeOut();
       		init();
	    },
	    error: function(response) {
	    	$('#table-update-overlay').fadeOut();
	    }
    });
}

function insertInventoryEntry() {
	$('#table-update-overlay').fadeIn();
    $.ajax({
       	type: 'POST',
       	url: 'insert-inventory-entry',
       	data: $('#insert-inventory-entry').serialize(),
       	success: function(response) {
       		displaySnackbar(response['message']);
       		getInventoryEntries();
       		clearModal();
	    },
	    error: function(response) {
	    	displaySnackbar(JSON.parse(response['responseText'])['message']);
	    	getInventoryEntries();
	    }
    });
}

function updateInventoryEntries(queries) {
	if (Object.keys(queries).length > 0) {
		$('#table-update-overlay').fadeIn();
	    $.ajax({
	       	type: 'PUT',
	       	url: 'update-inventory-entries',
	       	data: $.param(queries),
	       	success: function(response) {
	       		displaySnackbar(response['message']);
	       		getInventoryEntries();
	       		clearModal();
		    }
	    });
	}
}

function deleteInventoryEntries(ids) {
	if (ids.length > 0) {
		$('#table-update-overlay').fadeIn();
		$.ajax({
	       	type: 'DELETE',
	       	url: 'delete-inventory-entries',
	       	data: 'ids[]=' + ids.join('&ids[]='),
	       	success: function(response) {
	       		displaySnackbar(response['message']);
	       		getInventoryEntries();
		    }
	    });
	}
}

function transformIntoInput(checkbox, className) {
	var td = checkbox.parent().parent().find(className);
	const text = td.text();
	var input = $(document.createElement('input'));
	input.attr('value', text);
	input.attr('type', 'text');
	input.attr('class', 'edit-entry');
	td.html(input);
}

function transformIntoSelect(checkbox, className) {
	var td = checkbox.parent().parent().find(className);
	const selectedOption = td.text().trim();
	var select = $(document.createElement('select'));

	if (className === '.status') {
		var inStock = createOption('In stock', selectedOption);
		var ordered = createOption('Ordered', selectedOption);
		var sold = createOption('Sold', selectedOption);
		select.append(inStock);
		select.append(ordered);
		select.append(sold);
	} else if (className === '.booked' || className === '.listed') {
		var yes = createOption('Yes', selectedOption);
		var no = createOption('No', selectedOption);
		select.append(yes);
		select.append(no);
	}

	select.attr('class', 'edit-entry');

	td.html(select);
}

function createOption(value, selectedValue) {
	var option = $(document.createElement('option'));
	option.html(value);
	option.val(value);

	if (value === selectedValue) {
		option.attr('selected', true);
	}
	return option;
}

function getFilterValues() {
	var filter = {};

	if ($('#id-filter-checkbox').prop('checked')) {
		const selectedValue = $('#id-filter-input').val();
		if (selectedValue.length > 0) {
			filter['id'] = selectedValue;
		}
	}

	if ($('#vin-nr-filter-checkbox').prop('checked')) {
		const selectedValue = $('#vin-nr-filter-input').val();
		if (selectedValue.length > 0) {
			filter['vin-nr'] = selectedValue;
		}
	}

	if ($('#model-filter-checkbox').prop('checked')) {
		const selectedValue = $('#model-filter-input').val();
		if (selectedValue.length > 0) {
			filter['model'] = selectedValue;
		}
	}

	if ($('#maker-filter-checkbox').prop('checked')) {
		const selectedValue = $('#maker-filter-input').val();
		if (selectedValue.length > 0) {
			filter['maker'] = selectedValue;
		}
	}

	if ($('#year-filter-checkbox').prop('checked')) {
		const selectedValues = $('#year-filter-slider').bootstrapSlider('getValue');
		filter['year-low'] = selectedValues[0];
		filter['year-high'] = selectedValues[1];
	}

	if ($('#msrp-filter-checkbox').prop('checked')) {
		const selectedValues = $('#msrp-filter-slider').bootstrapSlider('getValue');
		filter['msrp-low'] = selectedValues[0];
		filter['msrp-high'] = selectedValues[1];
	}

	if ($('#status-filter-checkbox').prop('checked')) {
		const selectedValue = $('#status-filter-select').prop('selectedIndex');
		if (selectedValue > 0) {
			filter['status'] = $('#status-filter-select option:selected').text();
		}
	}

	if ($('#booked-filter-checkbox').prop('checked')) {
		const selectedValue = $('input[name=booked-filter-radio]:checked').val();
		if (selectedValue !== undefined) {
			filter['booked'] = selectedValue;
		}
	}

	if ($('#listed-filter-checkbox').prop('checked')) {
		const selectedValue = $('input[name=listed-filter-radio]:checked').val();
		if (selectedValue !== undefined) {
			filter['listed'] = selectedValue;
		}
	}

	return filter;
}

function clearFilter() {
	$('.inventory-filter-content input[type=text]').val('');
	$('.inventory-filter-content input[type=radio]').prop('checked', false);
	$('.inventory-filter-content input[type=checkbox]').prop('checked', false);
	$('.inventory-filter-content select').prop('selectedIndex', 0);
	$('#year-filter-slider').bootstrapSlider('setValue', [1990, 2017]);
	$('#msrp-filter-slider').bootstrapSlider('setValue', [0, 100000]);
}

function clearModal() {
	$('#myModal input[type=text]').val('');
	$('#myModal input[type=radio]').prop('checked', false);
	$('#myModal select').prop('selectedIndex', 0);
}

function displaySnackbar(message) {
	var snackbar = $(document.createElement('div'));
	snackbar.addClass('snackbar');
	snackbar.text(message);
	snackbar.hide();
	$('main').append(snackbar);
	snackbar
		.slideDown(500)
		.delay(5000)
		.slideUp(500);
}