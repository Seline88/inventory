@section('site-menu')
    <div class="site-menu-content">
        <div id="site-logo-container">
            <img src="http://1.bp.blogspot.com/-f7BI2C_JQ4w/Vb7sIGU-fkI/AAAAAAAAB3A/Phy8sCIqsVI/s640/car-logos-got2bsmart.jpg">
        </div>
        <ul>
            <li class="site-menu-item site-menu-item-selected">Inventory</li>
            <li class="site-menu-item">Commission</li>
            <li class="site-menu-item">Manage Market</li>
            <li class="site-menu-item">Manage Customer</li>
            <li class="site-menu-item">Report Setting</li>
            <li class="site-menu-item">Sign Out</li>
        </ul>
    </div>
    <div class="site-menu-button">
        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
            <path fill="#333" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />
        </svg>
    </div>
@endsection 