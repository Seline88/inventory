@if (isset($entries) && count($entries) > 0)
    @foreach ($entries as $entry)
        <tr>
            <td><input class="entry-checkbox" type="checkbox"></td>
            <td class="id">{{ $entry -> id }}</td>
            <td class="vin-nr">{{ $entry -> vin_nr }}</td>
            <td class="model">{{ $entry -> model }}</td>
            <td class="maker">{{ $entry -> maker }}</td>
            <td class="year">{{ $entry -> year }}</td>
            <td class="msrp">{{ $entry -> msrp }}</td>
            <td class="status">{{ ucfirst($entry -> status) }}</td>
            <td class="booked">@if ($entry -> booked == 1) Yes @else No @endif</td>
            <td class="listed">@if ($entry -> listed == 1) Yes @else No @endif</td>
        </tr>
    @endforeach
@else
    <tr>
    @if (isset($queryCount) && $queryCount > 0)
        <td class="no-data-available" colspan="10"><em>No data in the database matches the filter</em></td>
    @else
        <td class="no-data-available" colspan="10"><em>No data in the database</em></td>
    @endif
    </tr>
@endif