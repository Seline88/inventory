@extends('master')
@extends('site_inventory_filter')
@extends('site_menu')

@section('site-content')
    <main class="site-content-container">
        <section>
            <h1>Inventory</h1>
            <div id="table-inventory-container">
                <div id="table-update-overlay">
                    <svg id="update-spinner" viewBox="0 0 24 24" width="72" height="72">
                        <path fill="#000000" d="M17.65,6.35C16.2,4.9 14.21,4 12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20C15.73,20 18.84,17.45 19.73,14H17.65C16.83,16.33 14.61,18 12,18A6,6 0 0,1 6,12A6,6 0 0,1 12,6C13.66,6 15.14,6.69 16.22,7.78L13,11H20V4L17.65,6.35Z">
                            <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 12 12" to="360 12 12" dur="2s" repeatCount="indefinite"/>
                        </path>
                    </svg>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><input id="select-all-entries-checkbox" type="checkbox"></th>
                            <th>ID</th>
                            <th>VIN#</th>
                            <th>Model</th>
                            <th>Maker</th>
                            <th>Year</th>
                            <th>MSRP</th>
                            <th>Status</th>
                            <th>Booked</th>
                            <th>Listed</th>
                        </tr>
                    </thead>
                    <tbody id="inventory-entries">
                        @include('inventory_entries')
                    </tbody>
                </table>
            </div>
            <div id="site-inventory-buttons-container">
                <a class="btn btn-success" href="#" role="button" data-toggle="modal" data-target="#myModal">Add</a>
                <a id="update-entries-button" class="btn btn-warning" href="#" role="button">Update</a>
                <a id="delete-entries-button" class="btn btn-danger" href="#" role="button">Delete</a>
                <label for="upload-file-button" class="btn btn-primary" href="#" role="button">
                    <span>Select file</span>
                </label>
                <input type="file" id="upload-file-button">

            </div>
        </section>
    </main>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form id="insert-inventory-entry" method="post">
                        
                        <label>VIN#</label>
                        <input name="vin_nr" type="text" class="form-control" aria-label>
                        <br>
                        <label>Model</label>
                        <input name="model" type="text" class="form-control" aria-label>
                        <br>
                        <label>Maker</label>
                        <input name="maker" type="text" class="form-control" aria-label>
                        <br>
                        <label>Year</label>
                        <input name="year" type="text" class="form-control" aria-label>
                        <br>
                        <label>MSRP</label>
                        <input name="msrp" type="text" class="form-control" aria-label>
                        <br>
                        <label>Status</label>
                        <select name="status" class="form-control" id="sel1">
                            <option disabled selected value style="display: none;"> -- Select a status -- </option>
                            <option>In stock</option>
                            <option>Ordered</option>
                            <option>Sold</option>
                        </select>
                        <br>
                        <label>Booked</label>
                        <br>
                        <label class="radio-inline">
                            <input type="radio" name="booked" id="inlineRadio1" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="booked" id="inlineRadio2" value="0"> No
                        </label>
                        <br>
                        <br>
                        <label>Listed</label>
                        <br>
                        <label class="radio-inline">
                            <input type="radio" name="listed" id="inlineRadio1" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="listed" id="inlineRadio2" value="0"> No
                        </label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="insertInventoryEntry()">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection 