@section('site-inventory-filter')
    <div class="inventory-filter-content">
        <h3>Inventory filter</h3>
        <div class="checkbox">
            <label><input id="id-filter-checkbox" type="checkbox">ID</label>
        </div>
        <input id="id-filter-input" type="text" class="form-control" aria-label>
        <br>
        <div class="checkbox">
            <label><input id="vin-nr-filter-checkbox"type="checkbox">VIN#</label>
        </div>
        <input id="vin-nr-filter-input" type="text" class="form-control" aria-label>
        <br>
        <div class="checkbox">
            <label><input id="model-filter-checkbox" type="checkbox">Model</label>
        </div>
        <input id="model-filter-input" type="text" class="form-control" aria-label>
        <br>
        <div class="checkbox">
            <label><input id="maker-filter-checkbox" type="checkbox">Maker</label>
        </div>
        <input id="maker-filter-input" type="text" class="form-control" aria-label>
        <br>
        <div class="checkbox">
            <label><input id="year-filter-checkbox" type="checkbox">Year</label>
        </div>
        <strong>1990</strong> <input style="width:158px" id="year-filter-slider" type="text" value="" data-slider-min="1990" data-slider-max="2017" data-slider-step="1" data-slider-value="[1990,2017]" /> <strong>2017</strong>
        <br>
        <br>
        <div class="checkbox">
            <label><input id="msrp-filter-checkbox" type="checkbox">MSRP</label>
        </div>
        <strong>0</strong> <input style="width:162px" id="msrp-filter-slider" type="text" value="" data-slider-min="0" data-slider-max="100000" data-slider-step="1000" data-slider-value="[0,100000]" /> <strong>100.000</strong>
        <br>
        <br>
        <div class="checkbox">
            <label><input id="status-filter-checkbox" type="checkbox">Status</label>
        </div>
        <select id="status-filter-select" name="status" class="form-control">
            <option disabled selected value style="display: none;"> -- Select a status -- </option>
            <option>In stock</option>
            <option>Ordered</option>
            <option>Sold</option>
        </select>
        <br>
        <div class="checkbox">
            <label><input id="booked-filter-checkbox" type="checkbox">Booked</label>
        </div>
        <label class="radio-inline">
            <input type="radio" name="booked-filter-radio" id="booked-filter-radio-yes" value="1"> Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="booked-filter-radio" id="booked-filter-radio-no" value="0"> No
        </label>
        <br>
        <br>
        <div class="checkbox">
            <label><input id="listed-filter-checkbox" type="checkbox">Listed</label>
        </div>
        <label class="radio-inline">
            <input type="radio" name="listed-filter-radio" id="listed-filter-radio-yes" value="1"> Yes
        </label>
        <label class="radio-inline">
            <input type="radio" name="listed-filter-radio" id="listed-filter-radio-no" value="0"> No
        </label>
        <br>
        <br>
        <a id="update-inventory-filter-button" class="btn btn-primary" href="#" role="button">Update filter</a>
        <a id="clear-inventory-filter-button" class="btn btn-danger" href="#" role="button">Clear filter</a>
    </div>
    <div class="inventory-filter-button">
        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
            <path fill="#333" d="M3,2H21V2H21V4H20.92L14,10.92V22.91L10,18.91V10.91L3.09,4H3V2Z" />
        </svg>
    </div>
@endsection 