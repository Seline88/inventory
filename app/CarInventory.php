<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarInventory extends Model {

    protected $table = 'car_inventory';
    public $timestamps = false;

}
