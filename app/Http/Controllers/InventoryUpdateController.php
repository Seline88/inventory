<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CarInventory;

class InventoryUpdateController extends Controller {
    
	public function update(Request $request) {
		$entries = $request->all();

		$ids = array_keys($entries);

		$message = 'Successfully updated entries with ID: ' . implode(', ', $ids);

		foreach ($entries as $id => $entry) {
			CarInventory::where('id', $id)->update([
				'vin_nr' => $entry['vin_nr'],
				'model' => $entry['model'],
				'maker' => $entry['maker'],
				'year' => $entry['year'],
				'msrp' => $entry['msrp'],
				'status' => $entry['status'],
				'booked' => $entry['booked'],
				'listed' => $entry['listed']
			]);
		}

     	return response()->json(array('message' => $message), 200);
	}

}
