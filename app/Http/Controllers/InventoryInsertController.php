<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CarInventory;

class InventoryInsertController extends Controller {
    
	public function insert(Request $request) {
        $vin_nr = $request->input('vin_nr');
        $model = $request->input('model');
        $maker = $request->input('maker');
        $year = $request->input('year');
        $msrp = $request->input('msrp');
        $status = $request->input('status');
        $booked = $request->input('booked');
        $listed = $request->input('listed');

        if (isset($vin_nr) && isset($model) && isset($maker) && isset($year) && isset($msrp) && isset($status) && isset($booked) && isset($listed)) {
        	$car_inventory = new CarInventory;

    		$car_inventory->vin_nr = $vin_nr;
    		$car_inventory->model = $model;
    		$car_inventory->maker = $maker;
    		$car_inventory->year = $year;
    		$car_inventory->msrp = $msrp;
    		$car_inventory->status = $status;
    		$car_inventory->booked = $booked;
    		$car_inventory->listed = $listed;

    		$car_inventory->save();

			$message = 'Successfully inserted entry with ID: ' . $car_inventory->id;
			$statusCode = 200;
        } else {
        	$message = 'Error inserting entry. Required fields missing.';
        	$statusCode = 400;
        }

     	return response()->json(array('message' => $message), $statusCode);
	}

}
