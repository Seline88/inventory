<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CarInventory;

class InventoryViewController extends Controller {
    
    public function getEntries(Request $request) {
    	$id = $request->input('id');
    	$vin_nr = $request->input('vin-nr');
    	$model = $request->input('model');
    	$maker = $request->input('maker');
    	$year_low = $request->input('year-low');
    	$year_high = $request->input('year-high');
    	$msrp_low = $request->input('msrp-low');
    	$msrp_high = $request->input('msrp-high');
    	$status = $request->input('status');
    	$booked = $request->input('booked');
    	$listed = $request->input('listed');

    	$whereClause = [];
        $queryCount = 0;
    	if (isset($id)) {
    		array_push($whereClause, ['id', '=', $id]);
            $queryCount++;
    	}
    	if (isset($vin_nr)) {
    		array_push($whereClause, ['vin_nr', '=', $vin_nr]);
            $queryCount++;
    	}
    	if (isset($model)) {
    		array_push($whereClause, ['model', '=', $model]);
            $queryCount++;
    	}
    	if (isset($maker)) {
    		array_push($whereClause, ['maker', '=', $maker]);
            $queryCount++;
    	}
    	if (isset($year_low)) {
    		array_push($whereClause, ['year', '>=', $year_low]);
            $queryCount++;
    	}
    	if (isset($year_high)) {
    		array_push($whereClause, ['year', '<=', $year_high]);
            $queryCount++;
    	}
    	if (isset($msrp_low)) {
    		array_push($whereClause, ['msrp', '>=', $msrp_low]);
            $queryCount++;
    	}
    	if (isset($msrp_high)) {
    		array_push($whereClause, ['msrp', '<=', $msrp_high]);
            $queryCount++;
    	}
    	if (isset($status)) {
    		array_push($whereClause, ['status', '=', $status]);
            $queryCount++;
    	}
    	if (isset($booked)) {
    		array_push($whereClause, ['booked', '=', $booked]);
            $queryCount++;
    	}
    	if (isset($listed)) {
    		array_push($whereClause, ['listed', '=', $listed]);
            $queryCount++;
    	}
    	
    	$entries = CarInventory::where($whereClause)->get();
    	
    	return view('inventory_entries', ['entries' => $entries, 'queryCount' => $queryCount]);
    }

}
