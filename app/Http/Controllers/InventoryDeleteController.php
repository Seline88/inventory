<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CarInventory;

class InventoryDeleteController extends Controller {
    
	public function delete(Request $request) {
		$ids = $request->input('ids');

		CarInventory::destroy((array)$ids);

		$message = 'Successfully deleted entries with ID: ' . implode(', ', $ids);

     	return response()->json(['message' => $message], 200);
	}

}
